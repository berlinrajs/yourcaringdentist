//
//  MedicalHistoryTableViewCell.swift
//  YourCaringDentist
//
//  Created by SRS Web Solutions on 28/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit
protocol MedicalHistoryCellDelegate {
    func radioButtonAction(sender : RadioButton)
}

class MedicalHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonYes: RadioButton!
    @IBOutlet weak var buttonNo: RadioButton!
    
    var delegate : MedicalHistoryCellDelegate?
    var question : PDQuestion!
    var option : PDOption!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell (obj : PDQuestion) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        question = obj
        labelTitle.text = obj.question
        buttonYes.selected = obj.selectedOption!
    }
    
    
    func configureCellOption (obj : PDOption) {
        backgroundColor = UIColor.clearColor()
        contentView.backgroundColor = UIColor.clearColor()
        option = obj
        labelTitle.text = obj.question
    }
    
    @IBAction func radioButtonAction(sender: RadioButton) {
        if question != nil {
            if question.isAnswerRequired == true && sender == buttonYes {
                self.delegate?.radioButtonAction(sender)
            } else {
                question.selectedOption = sender == buttonYes
            }
        } else if option != nil {
            option.isSelected = sender == buttonYes
            self.delegate?.radioButtonAction(sender)
        }
        
    }


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
