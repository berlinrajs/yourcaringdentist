//
//  PrivacyAlert.swift
//  PeopleCenter
//
//  Created by Bala Murugan on 6/1/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PrivacyAlert: UIView {

//    @IBOutlet weak var textviewEffort : UITextView!
    @IBOutlet weak var textviewAcknowledgement : UITextView!
    @IBOutlet weak var radioAcknowledgement : RadioButton!

    var completion:((Int,String,String,Bool)->Void)?
    var errorAlert : ((String) -> Void)?

    static var sharedInstance : PrivacyAlert {
        let instance :  PrivacyAlert = NSBundle.mainBundle().loadNibNamed("PrivacyAlert", owner: nil, options: nil).first as! PrivacyAlert
        return instance
        
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func showPopUp(inview : UIView ,completion : (acknowledgementTag : Int , acknowledgementReason : String , efforts : String , isOkSelected : Bool) -> Void , showAlert : (alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
//        textviewEffort.text = ""
        textviewAcknowledgement.text = ""
        radioAcknowledgement.deselectAllButtons()
        
        //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        inview.addSubview(self)
        inview.bringSubviewToFront(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }

    @IBAction func radioButtonTagged (sender : RadioButton){
        if sender.tag == 4{
            textviewAcknowledgement.userInteractionEnabled = true
            textviewAcknowledgement.alpha = 1.0
        }else{
            textviewAcknowledgement.userInteractionEnabled = false
            textviewAcknowledgement.alpha = 0.5
        }
    }
    
    @IBAction func okButtonPressed (sender : UIButton){
//        if  textviewEffort.isEmpty && radioAcknowledgement.selectedButton == nil{
//            self.errorAlert!("Please enter the efforts made to obtain written Acknowledgement or select any reason")
//        } else
        if radioAcknowledgement.selectedButton == nil {
            self.errorAlert!("PLEASE SELECT ANY REASON")
        } else if (radioAcknowledgement.selectedButton.tag == 4 && textviewAcknowledgement.isEmpty) {
            self.errorAlert!("PLEASE SPECIFY THE REASON")
        } else{
            let index = radioAcknowledgement.selectedButton == nil ? 0 : radioAcknowledgement.selectedButton.tag
            self.completion!(index, index == 4 ? textviewAcknowledgement.text! : "" , "", true)
            self.removeFromSuperview()
        }
    }
    
    @IBAction func skipButtonPressed (sender : UIButton){
        self.completion!(0, "" , "" , false)
        self.removeFromSuperview()
    }
}
