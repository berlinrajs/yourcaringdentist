//
//  AlertStudent.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AlertStudent: UIView {
    @IBOutlet weak var textfieldName : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    
    var completion : ((String,String) -> Void)!
    var cancelled : ((Bool,String) -> Void)!

    static var sharedInstance : AlertStudent {
        let instance :  AlertStudent = NSBundle.mainBundle().loadNibNamed("AlertStudent", owner: nil, options: nil).first as! AlertStudent
        return instance
    }
    
    func showPopUp(inView : UIView, completion: (schoolName: String , city : String) -> Void , cancelled : (showAlert : Bool , alertMessage : String) -> Void)  {
        self.completion = completion
        self.cancelled = cancelled
        inView.addSubview(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    @IBAction func okButtonPressed (sender : UIButton){
        if textfieldName.isEmpty{
            self.cancelled(true,"PLEASE ENTER THE SCHOOL NAME")
        }else if textfieldCity.isEmpty{
            self.cancelled(true,"PLEASE ENTER THE CITY")
        }else{
            self.completion(textfieldName.text! , textfieldCity.text!)
            self.removeFromSuperview()
        }
        
    }
    
    @IBAction func onCancelButtonPressed (Sender : UIButton){
        self.cancelled(false,"")
        self.removeFromSuperview()
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
