//
//  MedicationAlert.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 7/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicationAlert: UIView {

    //    @IBOutlet weak var textviewEffort : UITextView!
    @IBOutlet weak var textviewMedication : UITextView!
    @IBOutlet weak var textviewReason : UITextView!
    
    var completion:((String,String,Bool)->Void)?
    var errorAlert : ((String) -> Void)?
    
    static var sharedInstance : MedicationAlert {
        let instance :  MedicationAlert = NSBundle.mainBundle().loadNibNamed("MedicationAlert", owner: nil, options: nil).first as! MedicationAlert
        return instance
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func showPopUp(inview : UIView ,completion : ( medication : String , reason : String , isOkSelected : Bool) -> Void , showAlert : (alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        textviewReason.text = ""
        textviewMedication.text = ""
        
        inview.addSubview(self)
        inview.bringSubviewToFront(self)
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    
    @IBAction func okButtonPressed (sender : UIButton){
        if textviewMedication.isEmpty {
            self.errorAlert!("PLEASE ENTER THE MEDICATION DETAILS")
        } else if textviewReason.isEmpty  {
            self.errorAlert!("PLEASE SPECIFY THE REASON")
        } else{
            self.completion!( textviewMedication.text! , textviewReason.text, true)
            self.removeFromSuperview()
        }
    }
    
    @IBAction func skipButtonPressed (sender : UIButton){
        self.completion!( "" , "" , false)
        self.removeFromSuperview()
    }

}
