//
//  PDPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String?
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var signature1 : UIImage!
    var signature2 : UIImage!
    var signature3 : UIImage!
    var signature4 : UIImage!
    var signature5 : UIImage!
    var relationship: String?

    var privacyAcknowledgementTag : Int!
    var privacyAcknowledgementReason : String!
    var okPressed : Bool!

    var lastPhysicalExam : String!
    var isHavingMurmer : Bool!
    var isHavingCHF : Bool!
    var isHavingMVP : Bool!
    var isHavingArtificialValue : Bool!
    var isNeedsPaceMaker : Bool!
    var isDrugsOrMedication : Bool!
    var nameOfDrug : String!
    var medicationType : String!
    var medicalHistoryQuestions2 : [PDOption]!

    //TOOTH EXTRACTION
    var othersText1 : String?
    var othersText2 : String?
    var toothExtractionQuestions1 : String?
    var toothExtractionQuestions2 : String?
    var prognosisProcedure : String?

    //CHILD TREATMENT
    var selectedOptions : [String]!
    var otherTreatment : String?
    var treatmentDate : String?

    var fullName: String {
        return initial!.characters.count > 0 ? firstName + " " + initial! + " " + lastName : firstName + " " + lastName
    }
    
    var newPatient : NewPatient = NewPatient()
    var medicalHistory : MedicalHistory = MedicalHistory()
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
}

class NewPatient: NSObject {
    
    var socialSecurityNumber : String!
    var homePhone : String!
    var cellPhone : String!
    var workPhone : String!
    var address : String!
    var city : String!
    var state : String!
    var zipcode : String!
    var email : String!
    
    var genderTag : Int!
    var maritalStatusTag : Int!
    var employerName : String!
    var businessPhone : String!
    var hearAbout : String!
    var reference : String!
    var accountsTag : Int!
    var collegeStudentTag : Int!
    var schoolName : String!
    var schoolCity : String!
    
    var insuranceFirstName : String = ""
    var insuranceLastName : String = ""
    var insuranceInitial : String = ""
    var insuranceDOB : String = ""
    var insuranceRelationship : String = ""
    var insuranceSSN : String = ""
    var insuranceGroupNumber : String = ""
    var insuranceSubscriberId : String = ""
    var insuranceGroupName : String = ""
    
    var emergencyName : String!
    var emergencyPhone : String!
    var patientSignature : UIImage!
    var gaurdianSignature : UIImage!
    
    required override init() {
        super.init()
    }

}

class MedicalHistory: NSObject {
    
    var radioQuestions : [PDQuestion]!
    var medicalHistoryQuestions : [[PDQuestion]]!
    var upsetDetails : String = "N/A"
    var lastDentalVisit : String = "N/A"
    var orthodonticsDetails : String = "N/A"
    var gumTreatmentDetails : String = "N/A"
    var gumBleedDetails : String = "N/A"
    var swellingDetails : String = "N/A"
    var injuryDetails : String = "N/A"
    var poppingSensationDetails : String = "N/A"
    var clenchDetails : String = "N/A"
    var nightGaurdDetails : String = "N/A"
    var snoreDetails : String = "N/A"
    var snoreGaurdDetails : String = "N/A"
    var whiteningDetails : String = "N/A"
    
    var sensitiveOthers : String = "N/A"
    var sensitiveTag : Int = 0
    var toothBrushDetails : String = "N/A"
    var toothBrushTag : Int = 0
    var brushingFrequency : String = "N/A"
    var brushingFrequencyTag : Int = 0
    var flossingFrequency : String = "N/A"
    var flossingFrequencyTag : Int = 0
    var chiefComplaint : String = "N/A"
    var changeSmile : String = "N/A"
    
    
    var drugTag : Int = 0
    var medicationDetails : String = "N/A"
    var reasonForDrug : String = "N/A"
    var allergyTag : Int = 0
    var allergyReason : String = "N/A"
    var birthControl : String = "N/A"
    var pregnant : String = "N/A"
    var otherNotes : String = "N/A"
    var signaturePatient : UIImage!
    var signatureDoctor : UIImage!

    override init() {
        super.init()
        let quest1: [String] = ["Was there anything that upset you about your previous dentist or their office?",
                                "When was your last dental visit?",
                                "Have you ever had orthodontics (braces)?",
                                "Have you ever had gum treatment?",
                                "Do your gums bleed?",
                                "Do you have any sores, blisters, or swelling on your gums, lips, or cheeks?",
                                "Have you ever had an injury to your face or jaw?",
                                "Do you ever have clicking or popping sensation?",
                                "Do you grind or clench your teeth?",
                                "Do you wear a Nightguard?",
                                "Do you Snore?",
                                "Do you wear a Snore guard?",
                                "Would you be interested in the latest, inexpensive way of whitening your teeth?"]
        self.radioQuestions = PDQuestion.arrayOfQuestions(quest1)
        for ques in self.radioQuestions{
            ques.isAnswerRequired = true
        }
        
        let ques2 : [String] = ["Anemia",
                                "Prosthetic Joint",
                                "AIDS or ARC",
                                "Fainting Spells",
                                "Asthma/ Hay Fever",
                                "Tuberculosis",
                                "High Blood Pressure",
                                "Diabetes",
                                "Hepatitis",
                                "GI Disorder",
                                "Seizures",
                                "Epilepsy",
                                "Sinus Trouble",
                                "Rheumatic Fever",
                                "Kidney Disease",
                                "Ever taken Phen-Fen",
                                "Jaundice",
                                "Acid Reflux",
                                "Ulcers",
                                "Immune Disorders"]
        
        let ques3 : [String] = ["Tumors",
                                "Arthritis/ Rheumatism",
                                "Allergies",
                                "Ever taken Fosamax",
                                "Liver Disease",
                                "Respiratory Disease",
                                "Excessive Bleeding",
                                "Chemical Dependency",
                                "Cancer",
                                "Radiation/Chemotherapy",
                                "Stroke",
                                "Other"]
        
        self.medicalHistoryQuestions = [PDQuestion.arrayOfQuestions(ques2),PDQuestion.arrayOfQuestions(ques3)]
        self.medicalHistoryQuestions[1].last!.isAnswerRequired = true

    }
}
