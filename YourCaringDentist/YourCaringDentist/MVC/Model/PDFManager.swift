//
//  PDFManager.swift
//  FutureDentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let kKeychainItemLoginName = "YourCaringDentist: Google Login"
let kKeychainItemName = "YourCaringDentist: Google Drive"
let kClientId = "995418654156-f975edr0ulmulq8mq6e5muv1nfo8s880.apps.googleusercontent.com"
let kClientSecret = "1usqYsv-0fNexyfm7STTdC27"
private let kFolderName = "YourCaringDentist"

class PDFManager: NSObject {
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    
    
    private func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            
            // Have the service object set tickets to fetch consecutive pages
            // of the feed so we do not need to manually fetch them.
            service.shouldFetchNextPages = true
            
            // Have the service object set tickets to retry temporary error conditions
            // automatically.
            service.retryEnabled = true
        }
        return service
    }
    
    
    func createPDFForView(view : UIView, fileName : String!, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        var image: UIImage?
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let _ = image {
            self.savePDF(UIImageJPEGRepresentation(image!, 1.0)!, fileName: fileName, patient: patient, completionBlock: completionBlock)
        }
    }
    
    
    func createPDFForScrollView(scrollView : UIScrollView, fileName : String, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        var image: UIImage?
        scrollView.scrollEnabled = false
        scrollView.clipsToBounds = false
        let size: CGSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height)
        
        let savedContentOffset = scrollView.contentOffset
        
        UIGraphicsBeginImageContext(size)
        scrollView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        scrollView.contentOffset = savedContentOffset
        
        UIGraphicsEndImageContext()
        if let _ = image {
            self.savePDF(UIImageJPEGRepresentation(image!, 1.0)!, fileName: fileName, patient: patient, completionBlock: completionBlock)
        }
        scrollView.scrollEnabled = true
        scrollView.clipsToBounds = true
    }
    
    
    func savePDF(pdfData : NSData, fileName : String, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        do {
            // save as a local file
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            do {
                let fileManager = NSFileManager.defaultManager()
                let files = try fileManager.contentsOfDirectoryAtPath(documentsPath)
                for file in files {
                    let path = "\(documentsPath)/\(file)"
                    try fileManager.removeItemAtPath(path)
                }
            } catch  {
                
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MMM'_'dd'_'yyyy"
            let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
            let name = "\(patient.firstName)_\(patient.lastName)_\(dateString)_\(fileName.fileName).jpg"
            let path = "\(documentsPath)/\(name)"
            try pdfData.writeToFile(path, options: .DataWritingAtomic)
            self.uploadFileToDrive(path, fileName: name, subFolderName: patient.fullName.fileName)
            completionBlock(finished: true)
        } catch _ as NSError {
            completionBlock(finished: false)
        }
    }
    
    
    func uploadFileToDrive(path : String, fileName : String, subFolderName: String) {
        func uploadFile(identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = "image/jpeg"
            driveFile.originalFilename = fileName
            driveFile.name = fileName
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: NSData(contentsOfFile: path)!, MIMEType: "image/jpeg")
            let query = GTLQueryDrive.queryForFilesCreateWithObject(driveFile, uploadParameters: uploadParameters)
            query.addParents = identitifer
            
            self.driveService().executeQuery(query, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = NSFileManager.defaultManager()
                    if fileManager.fileExistsAtPath(path) {
                        do {
                            try fileManager.removeItemAtPath(path)
                        } catch  {
                            
                        }
                    }
                } else {
                }
            })
        }
        
        func createFolder(folderName : String, parent : [String]?, isDateFolder: Bool) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreateWithObject(folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    if parent == nil {
                        checkAndCreateDateFolder(folder)
                    } else {
                        if isDateFolder {
                            checkAndCreateSubFolder(folder)
                        } else {
                            uploadFile(folder.identifier)
                        }
                    }
                } else {
                    
                }
            })
        }
        
        func checkAndCreateSubFolder(folder : GTLDriveFile) {
            let folderName = subFolderName
            
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery.q = "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(folder.identifier)' in parents"
            
            self.driveService().executeQuery(folderDateQuery, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && childFolder.count > 0 {
                        let dateFolder = childFolder[0] as! GTLDriveFile
                        uploadFile(dateFolder.identifier)
                    } else {
                        createFolder(folderName, parent: [folder.identifier], isDateFolder: false)
                    }
                } else {
                }
            })
        }
        func checkAndCreateDateFolder(folder : GTLDriveFile) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let folderName = dateFormatter.stringFromDate(NSDate()).uppercaseString
            
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery.q = "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(folder.identifier)' in parents"
            
            self.driveService().executeQuery(folderDateQuery, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && childFolder.count > 0 {
                        let dateFolder = childFolder[0] as! GTLDriveFile
                        checkAndCreateSubFolder(dateFolder)
                    } else {
                        createFolder(folderName, parent: [folder.identifier], isDateFolder: true)
                    }
                } else {
                }
            })
        }
        
        let folderQuery = GTLQueryDrive.queryForFilesList()
        folderQuery.q = "mimeType = 'application/vnd.google-apps.folder' and trashed = false"
        
        self.driveService().executeQuery(folderQuery, completionHandler: { (ticker, folder, error) -> Void in
            if (error == nil) {
                let fileList = (folder as! GTLDriveFileList).files
                let name = "\(UIDevice.currentDevice().name)_" + kFolderName
                if fileList != nil {
                    let arrayFiltered = fileList.filter({ (driveFolders) -> Bool in
                        let folder = driveFolders as! GTLDriveFile
                        return folder.name == name
                    })
                    if arrayFiltered.count > 0 {
                        let folder = arrayFiltered[0] as! GTLDriveFile
                        checkAndCreateDateFolder(folder)
                        
                    } else {
                        createFolder(name, parent: nil, isDateFolder: false)
                    }
                } else {
                    createFolder(name, parent: nil, isDateFolder: false)
                }
            } else {
            }
        })
    }
    func authorizeDrive(view : UIView, completion:(success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
        if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(success: true)
        } else {
            let authViewController = GTMOAuth2ViewControllerTouch(scope: kGTLAuthScopeDriveFile, clientID: kClientId, clientSecret: kClientSecret, keychainItemName: kKeychainItemName, completionHandler: { (controller, auth, error) -> Void in
                controller.view.removeFromSuperview()
                if error == nil {
                    self.driveService().authorizer = auth
                    completion(success: true)
                } else {
                    completion(success: false)
                }
            })
            authViewController.view.frame = view.frame
            view.addSubview(authViewController.view)
        }
    }
    
}
