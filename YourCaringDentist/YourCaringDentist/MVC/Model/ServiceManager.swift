//
//  ServiceManager.swift
//  ProDental
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 ProDental. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    
    class func fetchDataFromService(baseUrlString: String, serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result: result!)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error: error)
        }
    }
//    class func loginWithUsername(userName: String, password: String, completion: (success: Bool, error: NSError?) -> Void) {
//        //http://mncell.com/mclogin/apploginapi.php
//        ServiceManager.fetchDataFromService("http://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": "mcdistinctive", "username": userName, "password": password], success: { (result) in
//            if (result["posts"]!)!["status"] as! String == "success" {
//                completion(success: true, error: nil)
//            } else {
//                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
//            }
//        }) { (error) in
//            completion(success: false, error: nil)
//        }
//    }
    class func postReview(name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: (success: Bool, error: NSError?) -> Void) {
        //https://alpha.mncell.com/review/app_review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php", parameters: ["patient_appkey": "mcYourCaring", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"]!!)["status"] as! String == "success" {
                completion(success: true, error: nil)
            } else {
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!!)["message"] as! String]))
            }
            }) { (error) in
                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
}
