//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kNewPatientForm = "WELCOME FORM"
let kMedicalHistory = "MEDICAL HISTORY FORM"
let kPrivacypractices = "NOTICE OF PRIVACY PRACTICES"
let kFinancialPolicy = "FINANCIAL POLICY"
let kConsentForms = "CONSENT FORMS"
let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kDentalImplants = "DENTAL IMPLANTS"
let kToothExtraction = "TOOTH EXTRACTION"
let kPartialDenture = "PARTIAL/DENTURE ADJUSTMENTS"
let kInofficeWhitening = "INOFFICE WHITENING"
let kTreatmentOfChild = "TREATMENT OF A MINOR CHILD"
let kEndodonticTreatment = "ENDODONTIC TREATMENT"
let kDentalCrown = "DENTAL CROWN"
let kOpiodForm = "OPIOID FORM"
let kGeneralConsent = "GENERAL CONSENT"
let KHipaaPatient = "HIPAA PATIENT CONSENT FORM"
let kRefusalDentalTreatment = "REFUSAL OF DENTAL TREATMENT"
let kFeedBack = "CUSTOMER REVIEW FORM"

let toothNumberRequired = [kToothExtraction, kDentalCrown, kEndodonticTreatment, kOpiodForm]


class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kNewPatientForm,kMedicalHistory,kInsuranceCard,kDrivingLicense,kConsentForms, kFeedBack]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: !isConnected, forms : formObj)
    }
    

    
    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 5 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kDentalImplants,kToothExtraction,kPartialDenture,kInofficeWhitening,kTreatmentOfChild,kEndodonticTreatment,kDentalCrown,kOpiodForm,kGeneralConsent,KHipaaPatient,kFinancialPolicy, kRefusalDentalTreatment, kPrivacypractices], isSubForm:  true)
            }
            if formObj.formTitle == kFeedBack {
                formObj.index = 20
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
