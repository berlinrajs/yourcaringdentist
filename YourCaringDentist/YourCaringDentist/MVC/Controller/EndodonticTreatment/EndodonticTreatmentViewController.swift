//
//  EndodonticTreatmentViewController.swift
//  South Minneapolis Walk-in Clinic
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EndodonticTreatmentViewController: PDViewController {

    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: PDLabel!
    
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonBack!.hidden = isFromPreviousForm
        labelName.text = "\(patient.firstName) \(patient.lastName)"
        signatureView.layer.cornerRadius = 3.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EndodonticTreatmentViewController.setDateOnLabel))
        tapGesture.numberOfTapsRequired = 1
        labelDate.addGestureRecognizer(tapGesture)
        
        labelDetails.text = labelDetails.text!.stringByReplacingOccurrencesOfString("kDENTISTNAME", withString: patient.dentistName)
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.blackColor()
    }

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signatureView.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView.signatureImage()
            let endodonticTreatmentFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentFormVC") as! EndodonticTreatmentFormViewController
            endodonticTreatmentFormVC.patient = patient
            self.navigationController?.pushViewController(endodonticTreatmentFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
