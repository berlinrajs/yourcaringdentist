//
//  EndodonticTreatmentFormViewController.swift
//  South Minneapolis Walk-in Clinic
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EndodonticTreatmentFormViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var labelDetails1: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday

        labelDetails1.text = labelDetails1.text!.stringByReplacingOccurrencesOfString("kDENTISTNAME", withString: patient.dentistName)
        
        var patientInfo = "Patient Name"
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kEndodonticTreatment
        }

        
        let patientName = getText("\(patient.firstName) \(patient.lastName)")
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " D.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        patientInfo = patientInfo + "\nTooth number \(getText(form[0].toothNumbers))"
        textRanges.append(patientInfo.rangeOfText(getText(form[0].toothNumbers)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(float: 0)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
