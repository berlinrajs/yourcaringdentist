//
//  NewPatient1ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var textfieldHomePhone : UITextField!
    @IBOutlet weak var textfieldWorkPhone : UITextField!
    @IBOutlet weak var textfieldCellPhone : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldCity : UITextField!
    @IBOutlet weak var textfieldState : UITextField!
    @IBOutlet weak var textfieldZipcode : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        StateListView.addStateListForTextField(textfieldState)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if textfieldHomePhone.isEmpty || textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldZipcode.text!.isZipCode{
            let alert = Extention.alert("PLEASE ENTER THE VALID ZIPCODE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if (!textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber) || (!textfieldCellPhone.isEmpty && !textfieldCellPhone.text!.isPhoneNumber) || !textfieldHomePhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            let alert = Extention.alert("PLEASE ENTER THE VALID EMAIL")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            patient.newPatient.socialSecurityNumber = textfieldSSN.isEmpty ? "" : textfieldSSN.text
            patient.newPatient.homePhone = textfieldHomePhone.text!
            patient.newPatient.workPhone = textfieldWorkPhone.isEmpty ? "" : textfieldWorkPhone.text!
            patient.newPatient.cellPhone = textfieldCellPhone.isEmpty ? "" : textfieldCellPhone.text!
            patient.newPatient.address = textfieldAddress.text!
            patient.newPatient.city = textfieldCity.text!
            patient.newPatient.state = textfieldState.text!
            patient.newPatient.zipcode = textfieldZipcode.text!
            patient.newPatient.email = textfieldEmail.isEmpty ? "" : textfieldEmail.text!
            let new2VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient2VC") as! NewPatient2ViewController
            new2VC.patient = self.patient
            self.navigationController?.pushViewController(new2VC, animated: true)

        }
    }
}

extension NewPatient1ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldHomePhone || textField == textfieldCellPhone || textField == textfieldWorkPhone{
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldZipcode{
            return textField.formatZipCode(range, string: string)
        }else if textField == textfieldSSN{
            return textField.formatNumbers(range, string: string, count: 9)
//            formatSocialSecurityNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
