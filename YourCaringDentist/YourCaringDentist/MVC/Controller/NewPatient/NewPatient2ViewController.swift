//
//  NewPatient2ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient2ViewController: PDViewController {

    @IBOutlet weak var radioSex : RadioButton!
    @IBOutlet weak var radioMaritalStatus : RadioButton!
    @IBOutlet weak var textfieldEmployerName : UITextField!
    @IBOutlet weak var textfieldBusinessPhone : UITextField!
   // @IBOutlet weak var radioReference : RadioButton!
    @IBOutlet weak var radioAccounts : RadioButton!
    @IBOutlet weak var radioStudent : RadioButton!
    @IBOutlet weak var textfieldReferredBy : UITextField!
    @IBOutlet weak var textfieldHearAbout : UITextField!
//    var referredBy : String = ""
//    var otherReference : String = ""
    var studentSchoolName : String = ""
    var studentSchoolCity : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if radioSex.selectedButton == nil || radioMaritalStatus.selectedButton == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldBusinessPhone.isEmpty && !textfieldBusinessPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            patient.newPatient.genderTag = radioSex.selectedButton.tag
            patient.newPatient.maritalStatusTag = radioMaritalStatus.selectedButton.tag
            patient.newPatient.employerName = textfieldEmployerName.isEmpty ? "" : textfieldEmployerName.text!
            patient.newPatient.businessPhone = textfieldBusinessPhone.isEmpty ? "" : textfieldBusinessPhone.text!
            patient.newPatient.hearAbout = textfieldHearAbout.isEmpty ? "N/A" : textfieldHearAbout.text!
            patient.newPatient.reference = textfieldReferredBy.isEmpty ? "N/A" : textfieldReferredBy.text!
            patient.newPatient.accountsTag = radioAccounts.selectedButton == nil ? 0 : radioAccounts.selectedButton.tag
            patient.newPatient.collegeStudentTag = radioStudent.selectedButton == nil ? 0 : radioStudent.selectedButton.tag
            patient.newPatient.schoolName = studentSchoolName
            patient.newPatient.schoolCity = studentSchoolCity
            YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE INSURANCE?", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1{
                    let new3VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient3VC") as! NewPatient3ViewController
                    new3VC.patient = self.patient
                    self.navigationController?.pushViewController(new3VC, animated: true)
                }else{
                    let new4VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient4VC") as! NewPatient4ViewController
                    new4VC.patient = self.patient
                    self.navigationController?.pushViewController(new4VC, animated: true)
                }
            })
        }
    }
    
    
    @IBAction func radioStudentAction (sender : RadioButton){
        if sender.tag == 1{
            AlertStudent.sharedInstance.showPopUp(self.view, completion: { (schoolName, city) in
                self.studentSchoolName = schoolName
                self.studentSchoolCity = city
                }, cancelled: { (showAlert, alertMessage) in
                    self.studentSchoolName = ""
                    self.studentSchoolCity = ""
                    sender.setSelectedWithTag(2)
                    if showAlert{
                        let alert = Extention.alert(alertMessage)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
            })
        }else{
            self.studentSchoolName = ""
            self.studentSchoolCity = ""
        }
    }
    
}

extension NewPatient2ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldBusinessPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

