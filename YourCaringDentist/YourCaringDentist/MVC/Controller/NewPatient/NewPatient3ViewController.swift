//
//  NewPatient3ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient3ViewController: PDViewController {
    
    @IBOutlet weak var textfieldFirstName : UITextField!
    @IBOutlet weak var textfieldLastName : UITextField!
    @IBOutlet weak var textfieldMiddleInitial : UITextField!
    @IBOutlet weak var textfieldDOB : UITextField!
    @IBOutlet weak var textfieldRelationship : UITextField!
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var textfieldGroupNumber : UITextField!
    @IBOutlet weak var textfieldSubscriberId : UITextField!
    @IBOutlet weak var textfieldGroupName : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber {
            let alert = Extention.alert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)

        }else{
        patient.newPatient.insuranceFirstName = textfieldFirstName.text!
        patient.newPatient.insuranceLastName = textfieldLastName.text!
        patient.newPatient.insuranceInitial = textfieldMiddleInitial.text!
        patient.newPatient.insuranceDOB = textfieldDOB.text!
        patient.newPatient.insuranceRelationship = textfieldRelationship.text!
        patient.newPatient.insuranceSSN = textfieldSSN.text!
        patient.newPatient.insuranceGroupNumber = textfieldGroupNumber.text!
        patient.newPatient.insuranceGroupName = textfieldGroupName.text!
        patient.newPatient.insuranceSubscriberId = textfieldSubscriberId.text!
        let new4VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient4VC") as! NewPatient4ViewController
        new4VC.patient = self.patient
        self.navigationController?.pushViewController(new4VC, animated: true)
        }
    }
    
    @IBAction func radioInsuredPersonAction (sender : RadioButton){
        if sender.tag == 1{
            textfieldFirstName.text = patient.firstName
            textfieldLastName.text = patient.lastName
            textfieldMiddleInitial.text = patient.initial
            textfieldDOB.text = patient.dateOfBirth
            textfieldRelationship.text = "PATIENT"
        }else{
            textfieldFirstName.text = ""
            textfieldLastName.text = ""
            textfieldMiddleInitial.text = ""
            textfieldDOB.text = ""
            textfieldRelationship.text = ""
        }
    }
}

extension NewPatient3ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldMiddleInitial {
            return textField.formatInitial(range, string: string)
        }else if textField == textfieldSSN{
            return textField.formatNumbers(range, string: string, count: 9)
        }else if textField == textfieldSubscriberId || textField == textfieldGroupNumber{
            return textField.formatNumbers(range, string: string, count: 100)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

