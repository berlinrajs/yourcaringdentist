//
//  NewPatient4ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/22/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient4ViewController: PDViewController {
    
    @IBOutlet weak var textfieldEmergencyName : UITextField!
    @IBOutlet weak var textfieldEmergencyPhone : UITextField!
    @IBOutlet weak var radioMinor : RadioButton!
    
    @IBOutlet var signaturePatient : SignatureView!
    @IBOutlet var signatureGaurdian : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        signaturePatient.userInteractionEnabled = false
        signaturePatient.alpha = 0.5
        signatureGaurdian.userInteractionEnabled = false
        signatureGaurdian.alpha = 0.5

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioMinorSelected (sender : RadioButton){
        if sender.selectedButton.tag == 1 {
            signaturePatient.userInteractionEnabled = false
            signaturePatient.alpha = 0.5
            signatureGaurdian.userInteractionEnabled = true
            signatureGaurdian.alpha = 1.0
            signaturePatient.clear()
        }else{
            signatureGaurdian.userInteractionEnabled = false
            signatureGaurdian.alpha = 0.5
            signaturePatient.userInteractionEnabled = true
            signaturePatient.alpha = 1.0
            signatureGaurdian.clear()
        }
    }
    
    @IBAction func onNextButtonPressed(sender : UIButton){
        if textfieldEmergencyName.isEmpty || textfieldEmergencyPhone.isEmpty || radioMinor.selectedButton == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !textfieldEmergencyPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if (radioMinor.selectedButton.tag == 1 && !signatureGaurdian.isSigned()) || (radioMinor.selectedButton.tag == 2 && !signaturePatient.isSigned()){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            patient.newPatient.emergencyName = textfieldEmergencyName.text!
            patient.newPatient.emergencyPhone = textfieldEmergencyPhone.text!
            patient.newPatient.patientSignature = radioMinor.selectedButton.tag == 2 ? signaturePatient.signatureImage() : UIImage()
            patient.newPatient.gaurdianSignature = radioMinor.selectedButton.tag == 1 ? signatureGaurdian.signatureImage() : UIImage()
            let patientForm : NewPatientFormViewController = self.storyboard?.instantiateViewControllerWithIdentifier("NewPatientFormVC") as! NewPatientFormViewController
            patientForm.patient = patient
            self.navigationController?.pushViewController(patientForm, animated: true)
        }
    }
}

extension NewPatient4ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldEmergencyPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
