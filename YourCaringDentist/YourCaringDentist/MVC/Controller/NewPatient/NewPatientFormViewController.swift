//
//  NewPatientFormViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientFormViewController: PDViewController {
    
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelSSN : UILabel!
    @IBOutlet weak var labelBirthDate : UILabel!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelMiddleInitial : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelWorkPhone : UILabel!
    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelState : UILabel!
    @IBOutlet weak var labelZipcode : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var radioGender : RadioButton!
    @IBOutlet weak var radioMaritalStatus : RadioButton!
    @IBOutlet weak var labelEmployerName : UILabel!
    @IBOutlet weak var labelBusinessPhone : UILabel!
    @IBOutlet weak var labelHearAbout : UILabel!
    @IBOutlet weak var radioHearAbout : RadioButton!
    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var radioColegeStudent : RadioButton!
    @IBOutlet weak var labelSchoolName : UILabel!
    
    @IBOutlet weak var labelInsuranceFirstName : UILabel!
    @IBOutlet weak var labelInsuranceLastName : UILabel!
    @IBOutlet weak var labelInsuranceMiddleInitial : UILabel!
    @IBOutlet weak var labelInsuranceRelationship : UILabel!
    @IBOutlet weak var labelInsuranceDOB : UILabel!
    @IBOutlet weak var labelInsuranceSSN : UILabel!
    @IBOutlet weak var labelInsuranceSubscriberId : UILabel!
    @IBOutlet weak var labelInsuranceGroupId : UILabel!
    @IBOutlet weak var labelInsuranceGroupName : UILabel!
    @IBOutlet weak var labelEmergencyDetails : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelPrintName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureGaurdian : UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadData()  {
        labelDate.text = patient.dateToday
        labelSSN.text = patient.newPatient.socialSecurityNumber.socialSecurityNumber
        labelBirthDate.text = patient.dateOfBirth
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelMiddleInitial.text = patient.initial
        labelHomePhone.text = patient.newPatient.homePhone
        labelWorkPhone.text = patient.newPatient.workPhone
        labelCellPhone.text = patient.newPatient.cellPhone
        labelAddress.text = patient.newPatient.address
        labelCity.text = patient.newPatient.city
        labelState.text = patient.newPatient.state
        labelZipcode.text = patient.newPatient.zipcode
        labelEmail.text = patient.newPatient.email
        radioGender.setSelectedWithTag(patient.newPatient.genderTag)
        radioMaritalStatus.setSelectedWithTag(patient.newPatient.maritalStatusTag)
        labelEmployerName.text = patient.newPatient.employerName
        labelBusinessPhone.text = patient.newPatient.businessPhone
        labelHearAbout.text = patient.newPatient.hearAbout
        radioHearAbout.setSelectedWithTag(patient.newPatient.accountsTag)
        labelReference.text = patient.newPatient.reference
        radioColegeStudent.setSelectedWithTag(patient.newPatient.collegeStudentTag)
        labelSchoolName.text = patient.newPatient.schoolName + " " + patient.newPatient.schoolCity
        
        labelInsuranceFirstName.text = patient.newPatient.insuranceFirstName
        labelInsuranceLastName.text = patient.newPatient.insuranceLastName
        labelInsuranceMiddleInitial.text = patient.newPatient.insuranceInitial
        labelInsuranceRelationship.text = patient.newPatient.insuranceRelationship
        labelInsuranceDOB.text = patient.newPatient.insuranceDOB
        labelInsuranceSSN.text = patient.newPatient.insuranceSSN.socialSecurityNumber
        labelInsuranceSubscriberId.text = patient.newPatient.insuranceSubscriberId
        labelInsuranceGroupId.text = patient.newPatient.insuranceGroupNumber
        labelInsuranceGroupName.text = patient.newPatient.insuranceGroupName
        labelEmergencyDetails.text = patient.newPatient.emergencyName + " - " + patient.newPatient.emergencyPhone
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelPrintName.text = patient.fullName
        signaturePatient.image = patient.newPatient.patientSignature
        signatureGaurdian.image = patient.newPatient.gaurdianSignature
    }

}
