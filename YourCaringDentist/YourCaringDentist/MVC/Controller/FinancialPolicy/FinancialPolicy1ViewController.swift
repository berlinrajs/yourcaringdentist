//
//  FinancialPolicy1ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 8/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialPolicy1ViewController: PDViewController {

    @IBOutlet weak var imageViewSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday!
        labelDetails.text = labelDetails.text!.stringByReplacingOccurrencesOfString("kDENTISTNAME", withString: "Dr. Dobee")
//            patient.dentistName.uppercaseString)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !imageViewSignature.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let finance : FinancialPolicyFormViewController = self.storyboard?.instantiateViewControllerWithIdentifier("FinancialFormVC") as! FinancialPolicyFormViewController
            finance.patient = patient
            finance.patientSign = imageViewSignature.signatureImage()
            self.navigationController?.pushViewController(finance, animated: true)
        }
    }


}
