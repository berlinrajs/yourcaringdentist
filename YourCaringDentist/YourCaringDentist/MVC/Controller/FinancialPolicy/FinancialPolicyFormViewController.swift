//
//  FinancialPolicyFormViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 8/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialPolicyFormViewController: PDViewController {

    var patientSign : UIImage!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var imageviewSign : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        imageviewSign.image = patientSign
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
