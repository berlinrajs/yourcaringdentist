//
//  PrivacyPracticesFormVc.swift
//  YourCaringDentist
//
//  Created by SRS Web Solutions on 23/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPracticesFormVc: PDViewController {
    
   
    @IBOutlet weak var radioButtonAcknowledgement: RadioButton!
    
    @IBOutlet var labelPatientName: UILabel!
    
    @IBOutlet var imageViewSignature: UIImageView!
    
    @IBOutlet var labelDate: UILabel!
    
    @IBOutlet var labelOthers: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if patient.privacyAcknowledgementTag == 0 {
            labelPatientName.text = patient.firstName + " " + patient.lastName
            labelDate.text = patient.dateToday
            imageViewSignature.image = patient.signature1
        }
        
        if patient.privacyAcknowledgementTag > 0 {
            labelPatientName.text = patient.firstName + " " + patient.lastName
            labelDate.text = patient.dateToday
            radioButtonAcknowledgement.setSelectedWithTag(patient.privacyAcknowledgementTag)
          labelOthers.text = patient.privacyAcknowledgementReason
        }
        

        
        
//      labelPatientName.text = ("\(patient.firstName) \(patient.lastName)")
//        imageViewSignature.image = patient.signature1
//        labelDate.text = patient.dateToday
//        radioButtonAcknowledgement.setSelectedWithTag(patient.privacyAcknowledgementTag)
//        labelOthers.text = patient.privacyAcknowledgementReason
  
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
