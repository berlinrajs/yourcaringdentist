//
//  PrivacyPractice1ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/21/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPractice1ViewController: PDViewController {
    
    @IBOutlet var imageViewSignature: SignatureView!
    
    @IBOutlet var labelDate: DateLabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday

        // Do any additional setup after loading the view.
    }

    @IBAction func buttonActionNext(sender: AnyObject)
    {
    if !imageViewSignature.isSigned()  {
    let alert = Extention.alert("PLEASE SIGN THE FORM")
    self.presentViewController(alert, animated: true, completion: nil)
    } else if !labelDate.dateTapped {
    let alert = Extention.alert("PLEASE SELECT DATE")
    self.presentViewController(alert, animated: true, completion: nil)
    } else {
        let nextViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("PrivacyPracticesFormVc") as! PrivacyPracticesFormVc
        patient.signature1 = imageViewSignature.image
        nextViewControllerObj.patient = patient
        self.navigationController?.pushViewController(nextViewControllerObj, animated: true)        
    }
    
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
