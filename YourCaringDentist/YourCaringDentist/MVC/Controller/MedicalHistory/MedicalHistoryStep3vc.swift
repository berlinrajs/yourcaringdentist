//
//  MedicalHistoryStep3vc.swift
//  YourCaringDentist
//
//  Created by SRS Web Solutions on 29/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryStep3vc: PDViewController {
    
    @IBOutlet var radioDrug: RadioButton!
    @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var radioWoman : RadioButton!
    @IBOutlet weak var textviewOthernotes : UITextView!
    @IBOutlet weak var signaturePatient : SignatureView!
//    @IBOutlet weak var signatureDoctor : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var viewWoman : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        radioWoman.setSelectedWithTag(2)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func radioDrugAction (sender : RadioButton){
        
        if sender.tag == 1{
            MedicationAlert.sharedInstance.showPopUp(self.view, completion: { (medication, reason, isOkSelected) in
                if isOkSelected{
                    self.patient.medicalHistory.reasonForDrug = reason
                    self.patient.medicalHistory.medicationDetails = medication
                }else{
                    sender.setSelectedWithTag(2)
                    self.patient.medicalHistory.reasonForDrug = "N/A"
                    self.patient.medicalHistory.medicationDetails = "N/A"
                }
                }, showAlert: { (alertMessage) in
                    let alert = Extention.alert(alertMessage)
                    self.presentViewController(alert, animated: true, completion: nil)
            })
        }else{
            self.patient.medicalHistory.reasonForDrug = "N/A"
            self.patient.medicalHistory.medicationDetails = "N/A"
        }
    }
    
    @IBAction func radioAllergyAction (sender : RadioButton){
        if sender.tag == 1{
            PopupTextField.sharedInstance.showWithTitle("", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.allergyReason = textField.text!
                }else{
                    self.patient.medicalHistory.allergyReason = ""
                    sender.setSelectedWithTag(2)
                }
            })
        }else{
            self.patient.medicalHistory.allergyReason = ""
        }
        
    }
    
    @IBAction func radioBirthControlAction (sender : RadioButton){
        if sender.tag == 1{
            patient.medicalHistory.birthControl = "YES"
        }else{
            patient.medicalHistory.birthControl = "NO"
            YesOrNoAlert.sharedInstance.showWithTitle("Are you Pregnant?", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                if buttonIndex == 1{
                    self.patient.medicalHistory.pregnant = "YES"
                }else{
                    self.patient.medicalHistory.pregnant = "NO"
                }
            })
        }
        
    }
    @IBAction func radioWomanAction (sender : RadioButton){
        if sender.tag == 1{
            viewWoman.userInteractionEnabled = true
            viewWoman.alpha = 1.0
        }else{
            viewWoman.userInteractionEnabled = false
            viewWoman.alpha = 0.5
            self.patient.medicalHistory.pregnant = ""
            patient.medicalHistory.birthControl = ""
            
        }
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if radioAllergy.selectedButton == nil || radioDrug.selectedButton == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else if radioWoman.selectedButton.tag == 1 && radioBirthControl.selectedButton == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
//        }else if !signatureDoctor.isSigned() || !signaturePatient.isSigned(){
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)

        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
        patient.medicalHistory.drugTag = radioDrug.selectedButton.tag
        patient.medicalHistory.allergyTag = radioAllergy.selectedButton.tag
        patient.medicalHistory.otherNotes = textviewOthernotes.text! == "OTHER NOTES" ? "" : textviewOthernotes.text!
        patient.medicalHistory.signaturePatient = signaturePatient.signatureImage()
//        patient.medicalHistory.signatureDoctor = signatureDoctor.signatureImage()
        let medical = self.storyboard!.instantiateViewControllerWithIdentifier("MedicalHistory1VC") as! MedicalHistory1ViewController
        medical.patient = self.patient
        self.navigationController?.pushViewController(medical, animated: true)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension MedicalHistoryStep3vc : UITextViewDelegate {
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if textView.text == "OTHER NOTES" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "OTHER NOTES"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

