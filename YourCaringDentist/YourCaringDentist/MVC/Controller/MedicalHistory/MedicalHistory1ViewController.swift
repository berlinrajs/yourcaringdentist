//
//  MedicalHistory1ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory1ViewController: PDViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonAnswered: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionAnswered(sender: UIButton) {
        sender.selected = !sender.selected
    }

    @IBAction func onNextButtonPressed (sender : UIButton){
        if buttonAnswered.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            let medical = self.storyboard!.instantiateViewControllerWithIdentifier("MedicalHistory2VC") as! MedicalHistory2ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
            

        }
    }

}

extension MedicalHistory1ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 47
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.radioQuestions.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PatientInfoCell1", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.configCell(self.patient.medicalHistory.radioQuestions[indexPath.row])
        cell.delegate = self
        cell.tag = indexPath.row
        return cell
    }
}

extension MedicalHistory1ViewController : PatientInfoCellDelegate{
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        switch cell.tag {
        case 0:
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.upsetDetails = textView.text!
                }else{
                    self.patient.medicalHistory.upsetDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 1:
            PopupTextField.sharedInstance.showDatePopupWithTitle("When was your last Dental visit?", placeHolder: "PLEASE SELECT DATE", minDate: nil, maxDate: nil, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.lastDentalVisit = textField.text!
                }else{
                    self.patient.medicalHistory.lastDentalVisit = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)

                }
            })
            break
        case 2 :
            PopupTextField.sharedInstance.showWithTitle("Have you ever had orthodontics (braces)?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.orthodonticsDetails = textField.text!
                }else{
                    self.patient.medicalHistory.orthodonticsDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 3:
            PopupTextField.sharedInstance.showDatePopupWithTitle("Have you ever had gum treatment?", placeHolder: "PLEASE SELECT DATE", minDate: nil, maxDate: nil, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.gumTreatmentDetails = textField.text!
                }else{
                    self.patient.medicalHistory.gumTreatmentDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 4 :
            PopupTextField.sharedInstance.showWithTitle("Do your gums bleed?", placeHolder: "WHEN?", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.gumBleedDetails = textField.text!
                }else{
                    self.patient.medicalHistory.gumBleedDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 5:
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.swellingDetails = textView.text!
                }else{
                    self.patient.medicalHistory.swellingDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 6 :
            PopupTextField.sharedInstance.showWithTitle("Have you ever had an injury to your face or jaw?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.injuryDetails = textField.text!
                }else{
                    self.patient.medicalHistory.injuryDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
         case 7 :
            PopupTextField.sharedInstance.showWithTitle("Do you ever have clicking or popping sensation?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.poppingSensationDetails = textField.text!
                }else{
                    self.patient.medicalHistory.poppingSensationDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 8 :
            PopupTextField.sharedInstance.showWithTitle("Do you grind or clench your teeth?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.clenchDetails = textField.text!
                }else{
                    self.patient.medicalHistory.clenchDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 9 :
            PopupTextField.sharedInstance.showWithTitle("Do you wear a Nightguard?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.nightGaurdDetails = textField.text!
                }else{
                    self.patient.medicalHistory.nightGaurdDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 10 :
            PopupTextField.sharedInstance.showWithTitle("Do you Snore?", placeHolder: "PLEASE TYPE HERE", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.snoreDetails = textField.text!
                }else{
                    self.patient.medicalHistory.snoreDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 11 :
            PopupTextField.sharedInstance.showWithTitle("Do you wear a Snore guard?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.snoreGaurdDetails = textField.text!
                }else{
                    self.patient.medicalHistory.snoreGaurdDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break
        case 12 :
            PopupTextField.sharedInstance.showWithTitle("Would you be interested in the latest, inexpensive way of whitening your teeth?", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.whiteningDetails = textField.text!
                }else{
                    self.patient.medicalHistory.whiteningDetails = "N/A"
                    cell.radioButtonYes.setSelectedWithTag(2)
                }
            })
            break

        default:
            break
        }
    }
}
