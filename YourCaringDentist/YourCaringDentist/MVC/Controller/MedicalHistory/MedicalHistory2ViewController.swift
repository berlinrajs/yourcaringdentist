//
//  MedicalHistory2ViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory2ViewController: PDViewController {

    @IBOutlet weak var radioSensitive : RadioButton!
    @IBOutlet weak var radioToothBrush : RadioButton!
    @IBOutlet weak var radioBrushingFrequency : RadioButton!
    @IBOutlet weak var radioFlossingFrequency : RadioButton!
    @IBOutlet weak var textfieldBrushingFrequency : UITextField!
    @IBOutlet weak var textfieldFlossingFrequency : UITextField!
    @IBOutlet weak var textviewChiefComplaint : UITextView!
    @IBOutlet weak var textviewTeethWhitening : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        radioBrushingFrequency.setSelectedWithTag(1)
        radioFlossingFrequency.setSelectedWithTag(1)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if radioToothBrush.selectedButton == nil || textfieldFlossingFrequency.isEmpty || textfieldBrushingFrequency.isEmpty || textviewChiefComplaint.isEmpty{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)
        }else{
            patient.medicalHistory.sensitiveTag = radioSensitive.selectedButton == nil ? 0 : radioSensitive.selectedButton.tag
            patient.medicalHistory.toothBrushTag = radioToothBrush.selectedButton.tag
            patient.medicalHistory.brushingFrequency = textfieldBrushingFrequency.text!
            patient.medicalHistory.flossingFrequency = textfieldFlossingFrequency.text!
            patient.medicalHistory.chiefComplaint = textviewChiefComplaint.text!
            patient.medicalHistory.changeSmile = textviewTeethWhitening.text == "PLEASE TYPE HERE" ? "" : textviewTeethWhitening.text!
            patient.medicalHistory.brushingFrequencyTag = radioBrushingFrequency.selectedButton.tag
            patient.medicalHistory.flossingFrequencyTag = radioFlossingFrequency.selectedButton.tag
            let medical = self.storyboard!.instantiateViewControllerWithIdentifier("MedicalHistoryFormVC") as! MedicalHistoryFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }

    }
    
    @IBAction func radioSensitiveAction  (sender : RadioButton){
        if sender.tag == 4{
            PopupTextField.sharedInstance.showWithTitle("", placeHolder: "PLEASE SPECIFY?", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.sensitiveOthers = textField.text!
                }else{
                    self.patient.medicalHistory.sensitiveOthers = ""
                    sender.deselectAllButtons()
                }
            })
        }else{
            self.patient.medicalHistory.sensitiveOthers = ""
        }
        
    }
    
    @IBAction func radioToothBrushAction (sender : RadioButton){
        if sender.tag == 2{
            PopupTextField.sharedInstance.showWithTitle("", placeHolder: "PLEASE SPECIFY?", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if isEdited{
                    self.patient.medicalHistory.toothBrushDetails = textField.text!
                }else{
                    self.patient.medicalHistory.toothBrushDetails = ""
                    sender.deselectAllButtons()
                }
            })
        }else{
            self.patient.medicalHistory.toothBrushDetails = ""
        }

    }

}

extension MedicalHistory2ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldBrushingFrequency  || textField == textfieldFlossingFrequency{
            return textField.formatNumbers(range, string: string, count: 2)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension MedicalHistory2ViewController : UITextViewDelegate {
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        return true
    }

    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}


