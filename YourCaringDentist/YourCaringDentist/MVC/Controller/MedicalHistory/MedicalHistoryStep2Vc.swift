//
//  MedicalHistoryStep2Vc.swift
//  YourCaringDentist
//
//  Created by SRS Web Solutions on 28/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryStep2Vc: PDViewController {
    
    @IBOutlet var buttonNext: PDButton!
  
    @IBOutlet var buttonBack1: PDButton!
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var buttonVerified: UIButton!

    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            buttonBack1.userInteractionEnabled = false
            buttonNext.userInteractionEnabled = false

            self.activityIndicator.startAnimating()
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1.userInteractionEnabled = true
                self.buttonNext.userInteractionEnabled = true
            }

        }
    }
    
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }

    @IBAction func buttonActionNext(sender: AnyObject) {
        if buttonVerified.selected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 1 {
                let medicalHistoryStep3VC = self.storyboard?.instantiateViewControllerWithIdentifier("MedicalHistoryStep3vc") as! MedicalHistoryStep3vc
                medicalHistoryStep3VC.patient = patient
                self.navigationController?.pushViewController(medicalHistoryStep3VC, animated: true)
            } else {
                buttonBack1.userInteractionEnabled = false
                buttonNext.userInteractionEnabled = false
                self.buttonVerified.selected = false
                self.activityIndicator.startAnimating()
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.4 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.userInteractionEnabled = true
                    self.buttonNext.userInteractionEnabled = true
                }
            }
        }
}

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MedicalHistoryStep2Vc : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalHistoryQuestions[selectedIndex].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PatientInfoCell
        
        cell.delegate = self
        cell.configCell(self.patient.medicalHistory.medicalHistoryQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension MedicalHistoryStep2Vc : PatientInfoCellDelegate{
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        PopupTextField.sharedInstance.showWithTitle("", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
            if isEdited{
                self.patient.medicalHistory.medicalHistoryQuestions[1].last!.answer = textField.text!
            }else{
                self.patient.medicalHistory.medicalHistoryQuestions[1].last!.answer = ""
                cell.radioButtonYes.setSelectedWithTag(2)
            }
        })

        
    }
}

