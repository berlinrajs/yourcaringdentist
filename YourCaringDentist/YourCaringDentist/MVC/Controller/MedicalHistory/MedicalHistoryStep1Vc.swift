//
//  MedicalHistoryStep1Vc.swift
//  YourCaringDentist
//
//  Created by SRS Web Solutions on 28/06/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryStep1Vc: PDViewController {

    @IBOutlet var textFieldLastDentalVisit: PDTextField!
    
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var toolbar: UIToolbar!
    
    @IBOutlet var radioButtonMurmer: RadioButton!
    
    @IBOutlet var radioButtonCHF: RadioButton!
    
    @IBOutlet var radioButtonMVP: RadioButton!
    
    @IBOutlet var radioButtonArtificialValve: RadioButton!
    
    @IBOutlet var radioButtonPacemaker: RadioButton!
    
    @IBOutlet var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.maximumDate = NSDate()
        textFieldLastDentalVisit.inputView = datePicker
        textFieldLastDentalVisit.inputAccessoryView = toolbar

        // Do any additional setup after loading the view.
    }
    
    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
        textFieldLastDentalVisit.resignFirstResponder()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    
    @IBAction func datePickerDateChanged(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
    }
    

    @IBAction func buttonActionNext(sender: AnyObject) {
        if textFieldLastDentalVisit.isEmpty {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.presentViewController(alert, animated: true, completion: nil)

        }else if !buttonVerified.selected {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
      let nextViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("MedicalHistoryStep2Vc") as! MedicalHistoryStep2Vc
         patient.isHavingMurmer = radioButtonMurmer.selectedButton.tag == 1
        patient.isHavingCHF = radioButtonCHF.selectedButton.tag == 1
        patient.isHavingMVP = radioButtonMVP.selectedButton.tag == 1
        patient.isHavingArtificialValue = radioButtonArtificialValve.selectedButton.tag == 1
        patient.isNeedsPaceMaker = radioButtonPacemaker.selectedButton.tag == 1
            patient.lastPhysicalExam = textFieldLastDentalVisit.text!
        nextViewControllerObj.patient = patient
        
       self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        
    }
    }
    
    @IBAction func buttonVerified(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
