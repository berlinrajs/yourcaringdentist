//
//  MedicalHistoryFormViewController.swift
//  YourCaringDentist
//
//  Created by Bala Murugan on 6/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: PDViewController {
    @IBOutlet var arrayLabelUpset : [UILabel]!
    @IBOutlet weak var labelLastDentalVisit : UILabel!
    @IBOutlet weak var labelOrthodontics : UILabel!
    @IBOutlet weak var labelgumTreatment : UILabel!
    @IBOutlet weak var labelGumTreatmentDate : UILabel!
    @IBOutlet weak var labelGumsBleed : UILabel!
    @IBOutlet weak var labelGumsBleedDate : UILabel!
    @IBOutlet var arrayLabelSwelling : [UILabel]!
    @IBOutlet weak var labelTeethSensitive : UILabel!
    @IBOutlet weak var radioTeethSensitive : RadioButton!
    @IBOutlet weak var labelInjury : UILabel!
    @IBOutlet weak var labelPoppingSensation : UILabel!
    @IBOutlet weak var labelClench : UILabel!
    @IBOutlet weak var labelNightGaurd : UILabel!
    @IBOutlet weak var labelSnore : UILabel!
    @IBOutlet weak var labelSnoreGaurd : UILabel!
    @IBOutlet weak var labelToothBrush : UILabel!
    @IBOutlet weak var radioToothBrush : RadioButton!
    @IBOutlet weak var labelBrushingFrequency : UILabel!
    @IBOutlet weak var labelBrushingFrequencyTime : UILabel!
    @IBOutlet weak var labelFlossingFrequency : UILabel!
    @IBOutlet weak var labelFlossingFrequencyTime : UILabel!
    @IBOutlet var arrayLabelChiefComplaint : [UILabel]!
    @IBOutlet var arraylabelChangeSmile : [UILabel]!
    @IBOutlet weak var labelTeethWhitening : UILabel!
    
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelPhysicalExam : UILabel!
    @IBOutlet weak var radioMurmur : RadioButton!
    @IBOutlet weak var radioCHF : RadioButton!
    @IBOutlet weak var radioMVP : RadioButton!
    @IBOutlet weak var radioArtificialValve : RadioButton!
    @IBOutlet weak var radioPacemaker : RadioButton!
    @IBOutlet weak var radioDrugs : RadioButton!
    @IBOutlet var arrayLabelMedicine : [UILabel]!
    @IBOutlet var arrayLabelReason : [UILabel]!
    @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var allergyReason : UILabel!
    @IBOutlet weak var labelBirthcontrol : UILabel!
    @IBOutlet weak var labelPregnant : UILabel!
    @IBOutlet var arrayLabelOtherNotes : [UILabel]!
    @IBOutlet weak var patientSignature : UIImageView!
//    @IBOutlet weak var doctorSignature : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
//    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet var arrayRadioButtons : [RadioButton]!
    @IBOutlet weak var labelOthers : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadData()  {
        patient.medicalHistory.upsetDetails.setTextForArrayOfLabels(arrayLabelUpset)
        labelLastDentalVisit.text = patient.medicalHistory.lastDentalVisit
        labelOrthodontics.text = patient.medicalHistory.orthodonticsDetails
        labelgumTreatment.text = patient.medicalHistory.gumTreatmentDetails.isEmpty ? "" : "YES"
        labelGumTreatmentDate.text = patient.medicalHistory.gumTreatmentDetails
        labelGumsBleed.text = patient.medicalHistory.gumBleedDetails.isEmpty ? "" : "YES"
        labelGumsBleedDate.text = patient.medicalHistory.gumBleedDetails
        patient.medicalHistory.swellingDetails.setTextForArrayOfLabels(arrayLabelSwelling)
        labelTeethSensitive.text = patient.medicalHistory.sensitiveOthers
        radioTeethSensitive.setSelectedWithTag(patient.medicalHistory.sensitiveTag)
        labelInjury.text = patient.medicalHistory.injuryDetails
        labelPoppingSensation.text = patient.medicalHistory.poppingSensationDetails
        labelClench.text = patient.medicalHistory.clenchDetails
        labelNightGaurd.text = patient.medicalHistory.nightGaurdDetails
        labelSnore.text = patient.medicalHistory.snoreDetails
        labelSnoreGaurd.text = patient.medicalHistory.snoreGaurdDetails
        labelToothBrush.text = patient.medicalHistory.toothBrushDetails
        radioToothBrush.setSelectedWithTag(patient.medicalHistory.toothBrushTag)
        labelBrushingFrequency.text = patient.medicalHistory.brushingFrequency
        labelBrushingFrequencyTime.text = patient.medicalHistory.brushingFrequencyTag == 1 ? "day" : "week"
        labelFlossingFrequency.text = patient.medicalHistory.flossingFrequency
        labelFlossingFrequencyTime.text = patient.medicalHistory.flossingFrequencyTag == 1 ? "day" : "week"
        patient.medicalHistory.chiefComplaint.setTextForArrayOfLabels(arrayLabelChiefComplaint)
        patient.medicalHistory.changeSmile.setTextForArrayOfLabels(arraylabelChangeSmile)
        labelTeethWhitening.text = patient.medicalHistory.whiteningDetails
        
        
        labelName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelPhysicalExam.text = patient.lastPhysicalExam
        radioMurmur.setSelected(patient.isHavingMurmer)
        radioCHF.setSelected(patient.isHavingCHF)
        radioMVP.setSelected(patient.isHavingMVP)
        radioArtificialValve.setSelected(patient.isHavingArtificialValue)
        radioPacemaker.setSelected(patient.isNeedsPaceMaker)
        radioDrugs.setSelectedWithTag(patient.medicalHistory.drugTag)
        patient.medicalHistory.medicationDetails.setTextForArrayOfLabels(arrayLabelMedicine)
        patient.medicalHistory.reasonForDrug.setTextForArrayOfLabels(arrayLabelReason)
        radioAllergy.setSelectedWithTag(patient.medicalHistory.allergyTag)
        allergyReason.text = patient.medicalHistory.allergyReason
        labelBirthcontrol.text = patient.medicalHistory.birthControl
        labelPregnant.text = patient.medicalHistory.pregnant
        patient.medicalHistory.otherNotes.setTextForArrayOfLabels(arrayLabelOtherNotes)
        patientSignature.image = patient.medicalHistory.signaturePatient
//        doctorSignature.image = patient.medicalHistory.signatureDoctor
        labelDate2.text = patient.dateToday
//        labelDate3.text = patient.dateToday
        
        let arrayQuestions : NSMutableArray = NSMutableArray()
        for ques in self.patient.medicalHistory.medicalHistoryQuestions[0]{
            arrayQuestions.addObject(ques)
        }
        for ques in self.patient.medicalHistory.medicalHistoryQuestions[1]{
            arrayQuestions.addObject(ques)
        }
        
        for btn in arrayRadioButtons{
            btn.setSelected((arrayQuestions[btn.tag] as! PDQuestion).selectedOption)
            
        }
        labelOthers.text = self.patient.medicalHistory.medicalHistoryQuestions[1].last!.answer
        
    }
    
}
