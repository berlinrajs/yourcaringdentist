//
//  DentalCrownViewController.swift
//  South Minneapolis Walk-in Clinic
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalCrownViewController: PDViewController {

    var textRanges : [NSRange]! = [NSRange]()

    
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var labelDate2: PDLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = "\(patient.firstName) \(patient.lastName)"

        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kDentalCrown
        }
        
        let patientName = getText("\(patient.firstName) \(patient.lastName)")
        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("FIRSTNAME LASTNAME", withString: patientName).stringByReplacingOccurrencesOfString("kToothNumber", withString:getText(form[0].toothNumbers)).stringByReplacingOccurrencesOfString("kDENTISTNAME", withString: patient.dentistName)
        self.buttonBack!.hidden = isFromPreviousForm
        
        signatureView1.layer.cornerRadius = 3.0
        signatureView2.layer.cornerRadius = 3.0
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(DentalCrownViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(DentalCrownViewController.setDateOnLabel2))
        tapGesture2.numberOfTapsRequired = 1
        labelDate2.addGestureRecognizer(tapGesture2)
        // Do any additional setup after loading the view.
    }
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.blackColor()
    }
    
    func setDateOnLabel2() {
        labelDate2.text = patient.dateToday
        labelDate2.textColor = UIColor.blackColor()
    }

    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" || labelDate2.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
            patient.signature2 = signatureView2.signatureImage()
            let dentalCrownFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownFormVC") as! DentalCrownFormViewController
            dentalCrownFormVC.patient = patient
            self.navigationController?.pushViewController(dentalCrownFormVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
