//
//  RefusalOfDentalTreatmentViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 3/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RefusalOfDentalTreatmentViewController: PDViewController {
    
    
    
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var labelDate1: PDLabel!
    @IBOutlet weak var textViewTreatment: PDTextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView1.layer.cornerRadius = 3.0
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(RefusalOfDentalTreatmentViewController.setDateOnLabel1))
        tapGesture1.numberOfTapsRequired = 1
        labelDate1.addGestureRecognizer(tapGesture1)
        
        labelText1.text = labelText1.text?.stringByReplacingOccurrencesOfString("kDoctorName", withString: patient.dentistName)
        labelText2.text = labelText2.text?.stringByReplacingOccurrencesOfString("kDoctorName", withString: patient.dentistName)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setDateOnLabel1() {
        labelDate1.text = patient.dateToday
        labelDate1.textColor = UIColor.blackColor()
    }
    
    func setDateOnLabel2() {
//        labelDate2.text = patient.dateToday
//        labelDate2.textColor = UIColor.blackColor()
    }
    
    
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        if textViewTreatment.text == "TYPE HERE *" || textViewTreatment.isEmpty {
            let alert = Extention.alert("PLEASE ENTER TREATMENT NAME")
            self.presentViewController(alert, animated: true, completion: nil)
//        } else if !signatureView1.isSigned() || !signatureView2.isSigned() {
//            let alert = Extention.alert("PLEASE SIGN THE FORM")
//            self.presentViewController(alert, animated: true, completion: nil)
        } else if labelDate1.text == "Tap to date" {
            let alert = Extention.alert("PLEASE SELECT DATE")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.signature1 = signatureView1.signatureImage()
//            patient.signature2 = signatureView2.signatureImage()
            patient.otherTreatment = textViewTreatment.text
            let kRefusalOfDentalTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kRefusalOfDentalFormVC") as! RefusalOfDentalTreatmentFormViewController
            kRefusalOfDentalTreatmentVC.patient = patient
            self.navigationController?.pushViewController(kRefusalOfDentalTreatmentVC, animated: true)
        }
    }
}


extension RefusalOfDentalTreatmentViewController : UITextViewDelegate {
    
    
    func textViewDidBeginEditing(textView: UITextView) {
        
        if textView.text == "TYPE HERE *" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "TYPE HERE *"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}