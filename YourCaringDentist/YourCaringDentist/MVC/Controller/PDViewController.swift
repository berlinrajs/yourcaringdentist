//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: PDPatient!
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    func showAlert(message: String) {
        let alert = Extention.alert(message)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    func showAlert(message: String, completion: (completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "YOUR CARING DENTIST", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Destructive) { (action) -> Void in
            completion(completed: true)
        }
        alertController.addAction(alertOkAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        buttonBack?.hidden = isFromPreviousForm && buttonSubmit == nil
        buttonSubmit?.backgroundColor = UIColor.greenColor()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "YOUR CARING DENTIST", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            if self.patient.selectedForms.first!.formTitle! == "NOTICE OF PRIVACY PRACTICES"{
                                self.patient.selectedForms.removeFirst()
                            }
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, fileName: self.patient.selectedForms.first!.formTitle!.fileName, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
                
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kNewPatientForm){
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("NewPatient1VC") as! NewPatient1ViewController
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kMedicalHistory){
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("MedicalHistoryStep1Vc") as! MedicalHistoryStep1Vc
            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }  else if formNames.contains(kDentalImplants) {
            let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
            dentalImplantVC.patient = self.patient
            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
        }else if formNames.contains(kToothExtraction) {
            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
            toothExtractionVC.patient = self.patient
            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
        }else if formNames.contains(kPartialDenture) {
            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
            dentureAdjustmentVC.patient = self.patient
            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
        }else if formNames.contains(kInofficeWhitening) {
            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
            inOfficeWhiteningVC.patient = self.patient
            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
        }else if formNames.contains(kTreatmentOfChild) {
            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
            childTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
        }else if formNames.contains(kEndodonticTreatment) {
            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
            endodonticTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
        }else if formNames.contains(kDentalCrown) {
            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
            dentalCrownVC.patient = self.patient
            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
        }else if formNames.contains(kOpiodForm) {
            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
            opiodVC.patient = self.patient
            self.navigationController?.pushViewController(opiodVC, animated: true)
        }else if formNames.contains(kGeneralConsent) {
            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
            generalVC.patient = self.patient
            self.navigationController?.pushViewController(generalVC, animated: true)
        }else if formNames.contains(KHipaaPatient) {
            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("HipaaPatientConsentVc") as! HipaaPatientConsentVc
            generalVC.patient = self.patient
            self.navigationController?.pushViewController(generalVC, animated: true)
        }
        else if formNames.contains(kFinancialPolicy){
            let finance = self.storyboard!.instantiateViewControllerWithIdentifier("Finance1VC") as! FinancialPolicy1ViewController
            finance.patient = self.patient
            self.navigationController?.pushViewController(finance, animated: true)
        } else if formNames.contains(kRefusalDentalTreatment) {
            let refusalOfDentalTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kRefusalOfDentalVC") as! RefusalOfDentalTreatmentViewController
            refusalOfDentalTreatmentVC.patient = self.patient
            self.navigationController?.pushViewController(refusalOfDentalTreatmentVC, animated: true)
        }else if formNames.contains(kPrivacypractices){
            if patient.privacyAcknowledgementTag > 0 {
                let privacyObj = self.storyboard!.instantiateViewControllerWithIdentifier("PrivacyPracticesFormVc") as! PrivacyPracticesFormVc
                privacyObj.patient = self.patient
                self.navigationController?.pushViewController(privacyObj, animated: true)
            } else{
                let privacyObj = self.storyboard!.instantiateViewControllerWithIdentifier("PrivacyPractice1ViewController") as! PrivacyPractice1ViewController
                privacyObj.patient = self.patient
                self.navigationController?.pushViewController(privacyObj, animated: true)
            }
        } else if formNames.contains(kFeedBack) {
            let privacyObj = self.storyboard!.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
            privacyObj.patient = self.patient
            self.navigationController?.pushViewController(privacyObj, animated: true)
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
}
