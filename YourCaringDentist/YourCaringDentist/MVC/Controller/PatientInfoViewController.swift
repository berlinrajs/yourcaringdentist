//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    
    @IBOutlet weak var dropDownDentistName: BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = patient.dateToday
        DateInputView.addDatePickerForTextField(textFieldDateOfBirth, minimumDate: nil, maximumDate: NSDate())
        
        dropDownDentistName.items = ["Dr. Ethan Harris", "Dr. Sheila Dobee", "Dr. Gassam Hawari", "Dr. Marlana Shile", "Dr. Vahidnia", "Dr. Pestana", "Dr. Sway", "Dr. Moavenian"]
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.dropDownDentistName.selected = false
    }
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if dropDownDentistName.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT DENTIST NAME")
        } else  if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER FIRST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER LAST NAME")
            self.presentViewController(alert, animated: true, completion: nil)
        } else if textFieldDateOfBirth.isEmpty {
            let alert = Extention.alert("PLEASE ENTER DATE OF BIRTH")
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = textFieldInitial.isEmpty ? "" : textFieldInitial.text
            patient.dateOfBirth = textFieldDateOfBirth.text
            patient.dentistName = dropDownDentistName.selectedOption!
            self.gotoNextForm()
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
extension PatientInfoViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldInitial {
            return textField.formatInitial(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
