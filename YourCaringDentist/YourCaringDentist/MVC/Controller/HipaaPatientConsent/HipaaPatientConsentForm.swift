//
//  HipaaPatientConsentForm.swift
//  SouthMinneapolis
//
//  Created by SRS Web Solutions on 27/06/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HipaaPatientConsentForm: PDViewController {
    @IBOutlet var imageViewSignature: UIImageView!
    
    @IBOutlet var labelRelationshipName: UILabel!
    
    @IBOutlet var imageViewSignature2: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        imageViewSignature.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        labelRelationshipName.text = patient.relationship

        // Do any additional setup after loading the view.
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backButtonAction (sender : UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


