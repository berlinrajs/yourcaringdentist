//
//  HipaaPatientConsentVc.swift
//  SouthMinneapolis
//
//  Created by SRS Web Solutions on 27/06/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HipaaPatientConsentVc: PDViewController {
    @IBOutlet var imageViewSignature: SignatureView!
    @IBOutlet var imageViewSignature2: SignatureView!
    @IBOutlet var textRelationship: PDTextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBack!.hidden = isFromPreviousForm
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonActionNext(sender: AnyObject) {
        self.view.endEditing(true)
        if !imageViewSignature.isSigned() || !imageViewSignature2.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            
            let nextViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("HipaaPatientConsentForm") as! HipaaPatientConsentForm
            patient.signature1 = imageViewSignature.image
            patient.signature2 = imageViewSignature2.image
            patient.relationship = textRelationship.text
            nextViewControllerObj.patient = patient
            self.navigationController?.pushViewController(nextViewControllerObj, animated: true)
        }
    }
    
    

    @IBAction func buttonActionBack(sender: AnyObject) {
    self.navigationController?.popViewControllerAnimated(true)
}
}
    
extension HipaaPatientConsentVc : UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

